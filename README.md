# Equipe de Testes

## Objetivos:
* Desenvolver casos de testes
* Automatizar os processos de testes
* Criar issues
* Acompanhar issues
* Manter dashboard de casos de uso e status de issues

## Grupo 3 - membros
* HITALLO FLAVYO LOPES DE OLIVEIRA - @HitalloOliveira (Líder)
* MARCELLO GONCALVES DE OLIVEIRA ALVES - @m.alves
* REINALDO ALBERNAZ DE OLIVEIRA - @reinaldoalbernaz
* GABRIEL BATISTA ALEIXO - @Gabriell13
* MARCOS CHEDIAK - @mchediak

## Tarefas da sprint 1 (25/03 a 31/03): 
* Documentar processo de testes básico
* Avaliar um framework de testes em nodejs
    * Demonstrar uso com exemplos